import React, { useEffect, useState, useRef, StrictMode } from 'react';
import { BrowserRouter as Router, Routes, Route, useLocation } from 'react-router-dom';
import { create } from 'zustand';
import { devtools } from 'zustand/middleware';
import { ToastContainer, toast } from 'react-toastify';
import Form from './views/Form';

import superImage from './assets/images/logo.png';
import soupVideo from './assets/videos/soup.mp4';
import './index.scss';
import 'react-toastify/dist/ReactToastify.css';
import messages from './assets/content/messages.json';
import Info from './views/Info';

function App() {
  const [modal, setModal] = useState('');
  const [showForm, setShowForm] = useState(false);
  const [showInfo, setShowInfo] = useState(false);
  const [messageIndex, setMessageIndex] = useState(0);
  const messagesRef = useRef();


  useEffect(() => {
    const interval = setInterval(() => {
      setMessageIndex(messageIndex < messages.length - 1 ? messageIndex + 1 : 0);
      messagesRef.current.style.top = `${messageIndex * -3}rem`;
    }, 3000);

    return (() => {
      clearInterval(interval);
    });
  }, [messageIndex]);

  return (
    <StrictMode>
      <Router>
        <Routes>
          <Route
            path="/*"
            element={(
              <div className="app">
                <video
                  className="background"
                  src={soupVideo}
                  muted
                  autoPlay
                  playsInline
                  preload="true"
                  loop
                />
                <div className="main">
                  <img src={superImage} alt="logo" className="logo" />
                  <div className="reason">
                    <p>send a soup to...</p>
                    <div className="carousel">
                      <div className="carousel-messages" ref={messagesRef}>
                        {messages.map(message => (
                          <p className="carousel-messages-message">{message}</p>
                        ))
                        }
                      </div>
                    </div>
                  </div>
                  <div className="action">
                    <button
                      type="button"
                      className="primary"
                      onClick={() => {
                        setShowForm(true);
                      }}
                    >
                      Send a friend a Soup
                    </button>
                    <button
                      type="button"
                      onClick={() => {
                        setShowInfo(true);
                      }}
                    >
                      What's the Deal?
                    </button>
                  </div>
                  <Form
                    showForm={showForm}
                    setShowForm={setShowForm}
                  />
                  <Info
                    showInfo={showInfo}
                    setShowInfo={setShowInfo}
                  />
                </div>
              </div>
            )}
          />
        </Routes>
      </Router>
      <ToastContainer />
    </StrictMode>
  );
}

export default App;
