import React from 'react';
import './index.scss';
import veganSoupPhoto from '../../assets/images/vegan-soup.png';
import chickenSoupPhoto from '../../assets/images/chicken-soup.png';

function Form(props) {
  const { showForm, setShowForm } = props;

  const submit = () => {
    console.log('submit');
  };

  return (
    <div className={`form ${showForm ? '' : 'hidden'}`}>
      <div className="form-body modal">
        <h4>What kind of Süper?</h4>
        <div className="form-body-type">
          <label htmlFor="chicken">Chicken Soup<input type="radio" name="chicken" /><img src={chickenSoupPhoto} alt="chicken soup" /></label>
          <label htmlFor="vegan">Vegan Soup<input type="radio" name="vegan" /><img src={veganSoupPhoto} alt="vegan soup" /></label>
        </div>
        <h4>Who are you sending this Süper to?</h4>
        <label htmlFor="name">Name</label>
        <input type="text" id="name" />
        <label htmlFor="address">Address</label>
        <input type="text" id="address" />
        <label htmlFor="city">City</label>
        <input type="text" id="city" />
        <textarea id="note" placeholder="Leave a Note for [name]" />
        <button type="button" className="primary" onClick={submit}>Sübmit</button>
      </div>
      <button type="button" className="close" onClick={() => { setShowForm(false); }}>×</button>

    </div>
  );
}

export default Form;
