import React from 'react';
import './index.scss';

function Info(props) {
  const { showInfo, setShowInfo } = props;

  const submit = () => {
    console.log('submit');
  };

  return (
    <div className={`info ${showInfo ? '' : 'hidden'}`}>
      <div className="info-main modal">
        <h4> Be a Süperhero!</h4>
        <p>Süper is for your friend, your mom, your colleague, or anyone else in your life that needs a soup!</p>
        <p>Joanne in Finance is sick and she was there for you when you sprained your ankle and couldn't walk your cat Mr. Sprinkles. Send Joanne a Süper!</p>
        <button type="button" className="close" onClick={() => { setShowInfo(false); }}>×</button>
      </div>
    </div>
  );
}

export default Info;
